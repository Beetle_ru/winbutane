### The WinButane for simple messaging between applications. ###

For beginning need create object of WinButane class:

```
#!c++


WinButane *wb = new WinButane();
```


Then you need to initialize the object:


```
#!c++

wb->Init();
```


If you want take a desired port for receiving a messages then you need bind a port:


```
#!c++

wb->Bind(1135, wbListener);
```


Else if you want receiving messages on random port you need set listener:


```
#!c++

wb->SetListener(wbListener);
```


Where the wbListener is a callback function for receiving messages:


```
#!c++

void wbListener(receiveInfo *ri) {
    ri->senderAddr; // address, look example
    ri->msg; // message buffer
    ri->len; // length message buffer
}
```


If you don't want receiving messages then more need nothing to do)

For transmitting messages the WinButane has two functions for asynchronous and synchronous transmitting:


```
#!c++

wb->SendAsyncTo(ipOrDomainName, destinationPort, messageBuffer, lengthMessageBuffer);
wb->SendTo(ipOrDomainName, destinationPort, messageBuffer, lengthMessageBuffer);
```
## MessageLite ##

For the convenience into WinButane added class for simple messages.
The MsgLite used the three terms, the command, the message, the type of message.

And as first you need create a object of the MessageLite class


```
#!c++

MessageLite *msg = new MessageLite();
```

Then fill fields and pack message


```
#!c++

int len = msg->SetCmd("Request")->SetMsg("Who?")->SetT("string")->Pack();
```
Now message ready for transmission:


```
#!c++

wb->SendAsyncTo("localhost", 1133, msg->GetPkg(), len);
```
After receiving a message you can getting values with a get functions:

```
#!c++
void wbListener(receiveInfo *ri) {
    MessageLite *msg = new MessageLite();
    msg->SetPkg(ri->msg, ri->len)->Unpack();
	
    printf("cmd: \"%s\"\nmsg: \"%s\"\n", msg->GetCmd(), msg->GetMsg());
    
    msg->SetCmd("Response")->SetMsg("Hello")->Pack();
    RESPONSE(ri, msg->GetPkg(), msg->GetPkgLen());
    
    return;
}
```