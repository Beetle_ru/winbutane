#include "winbutane.h"
#include <stdio.h>

UINT ListenProc(PVOID param) {
	connectionInfo *ci = (connectionInfo*)param;

	sockaddr_in senderAddr;
	memset(&senderAddr, 0, sizeof(senderAddr));
	int senderAddrSize = sizeof (senderAddr);
	const int bufLen = 65507;
	char buffer[bufLen] = {0};

	while (ci->isReceive) {
		int recvBytes = recvfrom(ci->sock, buffer, bufLen, 0, (SOCKADDR *) & senderAddr, &senderAddrSize);
		if (recvBytes == SOCKET_ERROR) {
			int err =  WSAGetLastError();
			printf("recvfrom function failed with error: %d\n", err);
			//return err;
			Sleep(100);
		} else {
			if (recvBytes > 0) {
				receiveInfo ri;
				ri.senderAddr = &senderAddr;
				ri.self = ci->self;
				ri.msg = buffer;
				ri.len = recvBytes;
				ri.params = ci->params;

				ci->callBack(&ri);

				for (int i = 0; i < recvBytes; i++) {
					buffer[i] = 0;
				}
			}
		}
	}
	return 0;
}


int WinButane::m_objCnt = 0;

int WinButane::Init() {
	if (WSAStartup(MAKEWORD(2, 2), &m_wsaData)) {
		int err =  WSAGetLastError();
		printf("WSAStartup error: %d\n", err);
		return err;
    }

	m_sock = INVALID_SOCKET;
	m_sock = socket(AF_UNSPEC, SOCK_DGRAM, IPPROTO_UDP);

	if (m_sock == INVALID_SOCKET) {
		int err =  WSAGetLastError();
		printf("socket function failed with error: %d\n", err);
		return err;
	}

	m_objCnt++;
	return 0;
}

int WinButane::SetListener(ReceiveCallbackFunct *callBack, void *params) {
	m_receiveCallBack = callBack;
	m_ci = new connectionInfo;
	m_ci->callBack = callBack;
	m_ci->sock = m_sock;
	m_ci->isReceive = true;
	m_ci->self = this;
	m_ci->params = params;
	m_listenThread = AfxBeginThread(ListenProc, m_ci);
	return 0;
}

int WinButane::SendTo(sockaddr_in *addrr, char *buffer, int length) {
	if(sendto(m_sock, buffer, length, 0, (SOCKADDR *)addrr, sizeof(sockaddr_in)) == SOCKET_ERROR) {
		int err =  WSAGetLastError();
		printf("sendto function failed with error: %d\n", err);
		return err;
	}
	return 0;
}

int WinButane::SendTo(const char *ipAddr, unsigned short port, char *buffer, int length) {
	sockaddr_in sendAddr;
	sendAddr.sin_family = AF_INET;
	sendAddr.sin_addr.s_addr = inet_addr(ipAddr);
	
	if (sendAddr.sin_addr.s_addr == INADDR_NONE) { 
		struct addrinfo *result = NULL;
		struct addrinfo *ptr = NULL;
		struct addrinfo hints;

		ZeroMemory( &hints, sizeof(hints) );
		hints.ai_family = sendAddr.sin_family;
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_protocol = IPPROTO_UDP;

		char portChr[10] = {0};
		itoa(port, portChr, 10);
		int ret = getaddrinfo(ipAddr, portChr, &hints, &result);

		if (ret) return ret;

		ptr = result;

		while(ptr) {
			if(ptr->ai_family == AF_INET) {
				struct sockaddr_in  *sockaddr_ipv4 = (struct sockaddr_in *) ptr->ai_addr;
				sendAddr.sin_addr.s_addr = sockaddr_ipv4->sin_addr.S_un.S_addr;
				break;
			}
			ptr = ptr->ai_next;
		}

		freeaddrinfo(result);
	}

	sendAddr.sin_port = htons(port);
	return SendTo(&sendAddr, buffer, length);
}

int WinButane::SendAsyncTo(const char *ipAddr, unsigned short port, char *buffer, int length) {
	sendInfo *si = (sendInfo*)malloc(sizeof(sendInfo));
	memset(si, 0, sizeof(sendInfo));
	
	int ipLen = strlen(ipAddr);

	si->self = this;
	si->ipAddr = (char*)malloc(ipLen + 1);
	si->port = port;
	si->buffer = (char*)malloc(length);
	si->length = length;
	memcpy(si->ipAddr, ipAddr, ipLen);
	memcpy(si->buffer, buffer, length);
	*(si->ipAddr + ipLen) = 0;

	AfxBeginThread(SendProc, si);

	return 0;
}

UINT SendProc(PVOID param) {
	sendInfo *si = (sendInfo*)param;
	
	si->self->SendTo(si->ipAddr, si->port, si->buffer, si->length);
	
	free(si->ipAddr);
	free(si->buffer);
	free(si);

	return 0;
}

int WinButane::Bind(unsigned short port) {
	sockaddr_in RecvAddr;
	RecvAddr.sin_family = AF_INET;
    RecvAddr.sin_port = htons(port);
    RecvAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(m_sock, (SOCKADDR *) & RecvAddr, sizeof (RecvAddr)) == SOCKET_ERROR) {
		int err =  WSAGetLastError();
		printf("bind function failed with error: %d\n", err);
		return err;
	}
	return 0;
}

int WinButane::Bind(unsigned short port, ReceiveCallbackFunct *callBack, void *params) {
	int result = Bind(port);
	if (!result) SetListener(callBack, params);
	return result;
}

WinButane::~WinButane() {
	m_ci->isReceive = false;

	if (m_listenThread != NULL) {
		DWORD exit_code= NULL;
		GetExitCodeThread(m_listenThread->m_hThread, &exit_code);
		if (exit_code == STILL_ACTIVE) {
			TerminateThread(m_listenThread->m_hThread, 0);
			CloseHandle(m_listenThread->m_hThread);
		}
	}

	delete m_listenThread;
	delete m_ci;
	
	closesocket(m_sock);

	if(m_objCnt >= 1) WSACleanup();
}